Simplex
=======

This is my personal game engine project.
My main goal with it is to experiment and learn about computer graphics and game engine development.

Supported platforms
-------------------
For now it only supports MacOS X, but I'll build it with multi-platform support in mind.

Modules
-------
Simplex is essentially a modular engine. 
The structure for each module is as follows:

  * simplex-[module-name]/src (Actual source code for the module)
  * simplex-[module-name]/include (include files for the module's classes)
  * simplex-[module-name]/tests (unit tests for the module's classes)
  * simplex-[module-name]/samples (samples that show the module's functionality)
  * simplex-[module-name]/docs (documentation on how to use the module or other stuff)
  * simplex-[module-name]/third-party (third party libraries that are used by this module alone)

Besides this, each module should include a README.md file that will list the external dependencies needed for that module to build.
Also, each module should include a CMakeLists.txt file.

Goals
-----

The immediate goals are (in order):
  * 3D Graphics Engine with OpenGL + GLSL
  * Win, Mac, Linux support
  
Future goals:
  * Ruby scripting using mruby
  * Asset manager
  * Lightmapping support
  * B-Spline-defined surfaces
  * 3D Animation Engine
  * Audio Engine
    * Support for wav, mp3, ogg
    * Support for Synthesis
    * DSP
  * 3D Audio Engine
  * Path-finding
  * AI with Behaviour Trees
  * IDE
  * 2D Graphics Engine
    * Support for custom shaders
    * 2D Animation Engine
  * iOS, Android support
  * Assets pipeline automation

Features:

  * Modular system
